package number

// ComplementInt16 根据补码计算真实数据
func ComplementInt16(number uint16) int16 {
	var result int16
	if number >= 32768 { // 如果大于0x8000，说明是负数，需要做一次补码运算
		result = int16(0 - (^number + 1))
	} else {
		result = int16(number)
	}

	return result
}
