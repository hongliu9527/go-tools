/*
 * @Author: hongliu
 * @Date: 2022-12-26 17:39:36
 * @LastEditors: hongliu
 * @LastEditTime: 2023-07-20 11:28:22
 * @FilePath: \go-tools\strings\strings.go
 * @Description: 字符串处理工具库
 *
 * Copyright (c) 2022 by 洪流, All Rights Reserved.
 */

package string_tool

import (
	"regexp"
	"strings"
	"unsafe"
)

var matchFirstCap = regexp.MustCompile("(.)([A-Z][a-z]+)")
var matchAllCap = regexp.MustCompile("([a-z0-9])([A-Z])")

// ToSnakeCase 小驼峰转下划线
func ToSnakeCase(str string) string {
	snake := matchFirstCap.ReplaceAllString(str, "${1}_${2}")
	snake = matchAllCap.ReplaceAllString(snake, "${1}_${2}")
	return strings.ToLower(snake)
}

func BytesToString(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}

func StringToBytes(s string) []byte {
	x := (*[2]uintptr)(unsafe.Pointer(&s))
	h := [3]uintptr{x[0], x[1], x[1]}
	return *(*[]byte)(unsafe.Pointer(&h))
}
