package process

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"strings"
)

// CheckProcessStatus 检查进程状态
func CheckProcessStatus(name string) (bool, error) {
	psOutput, err := exec.Command("ps", "-e").Output()
	if err != nil {
		return false, err
	}
	lowercaseOutput := bytes.ToLower(psOutput)

	scanner := bufio.NewScanner(bytes.NewReader(lowercaseOutput))
	for scanner.Scan() {
		line := scanner.Text()
		if strings.Contains(line, name) {
			return true, nil
		}
	}
	return false, nil
}

// KillProcess 根据进程名称关闭进程
func KillProcess(name string) error {
	psOutput, err := exec.Command("ps", "-e").Output()
	if err != nil {
		return err
	}
	lowercaseOutput := bytes.ToLower(psOutput)

	// 找到全部的进程信息
	names := make([]string, 0)
	scanner := bufio.NewScanner(bytes.NewReader(lowercaseOutput))
	for scanner.Scan() {
		line := scanner.Text()
		if strings.Contains(line, name) {
			names = append(names, line)
		}
	}

	for _, line := range names {
		pid := -1
		fmt.Sscanf(line, "%d", &pid)
		if pid > 0 {
			process, err := os.FindProcess(pid)
			if err != nil {
				return err
			}
			err = process.Kill()
			if err != nil {
				return err
			}
		}
	}
	return nil
}
