module gitee.com/hongliu9527/go-tools

go 1.19

require (
	github.com/bwmarrin/snowflake v0.3.0
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible
	github.com/rifflock/lfshook v0.0.0-20180920164130-b9218ef580f5
	github.com/sirupsen/logrus v1.8.1
	github.com/twinj/uuid v1.0.0
)

require (
	github.com/jonboulle/clockwork v0.3.0 // indirect
	github.com/lestrrat-go/strftime v1.0.6 // indirect
	github.com/myesui/uuid v1.0.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/sys v0.10.0 // indirect
	gopkg.in/stretchr/testify.v1 v1.2.2 // indirect
)
