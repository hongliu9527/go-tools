package modbus

// ModbusMap 初始化modbus指令
var ModbusMap map[string]uint8

func init() {
	ModbusMap = make(map[string]uint8)

	ModbusMap["01"] = 1
	ModbusMap["02"] = 2
	ModbusMap["03"] = 3
	ModbusMap["04"] = 4
	ModbusMap["05"] = 5
	ModbusMap["06"] = 6
	ModbusMap["0F"] = 15
	ModbusMap["10"] = 16
}
