package set

// CreateSet 根据切片创建集合
func CreateSet[T comparable](data []T) []T {
	dataMap := make(map[T]struct{})

	for _, value := range data {
		dataMap[value] = struct{}{}
	}

	result := make([]T, 0, len(dataMap))
	for elem := range dataMap {
		result = append(result, elem)
	}

	return result
}
