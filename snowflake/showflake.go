package snowflake

import "github.com/bwmarrin/snowflake"

var nodeMap map[int]*snowflake.Node

func init() {
	nodeMap = make(map[int]*snowflake.Node)
}

// CreateID 创建雪花算法ID
func CreateID(nodeID int) (uint64, error) {
	if node, ok := nodeMap[nodeID]; ok {
		id := node.Generate()
		return uint64(id), nil
	}

	newNode, err := snowflake.NewNode(int64(nodeID))
	if err != nil {
		return 0, err
	}

	id := newNode.Generate()
	return uint64(id), nil
}
