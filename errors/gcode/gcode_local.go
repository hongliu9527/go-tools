/*
 * @Author: hongliu
 * @Date: 2022-12-29 10:51:12
 * @LastEditors: hongliu
 * @LastEditTime: 2022-12-29 14:49:23
 * @FilePath: \go-tools\errors\gcode\gcode_local.go
 * @Description:外部错误处理包具体实现
 *
 * Copyright (c) 2022 by 洪流, All Rights Reserved.
 */

package gcode

import (
	"fmt"
)

// localCode is an implementer for interface Code for internal usage only.
type localCode struct {
	code    int         // Error code, usually an integer.
	message string      // Brief message for this error code.
	detail  interface{} // As type of interface, it is mainly designed as an extension field for error code.
}

// Code returns the integer number of current error code.
func (c localCode) Code() int {
	return c.code
}

// Message returns the brief message for current error code.
func (c localCode) Message() string {
	return c.message
}

// Detail returns the detailed information of current error code,
// which is mainly designed as an extension field for error code.
func (c localCode) Detail() interface{} {
	return c.detail
}

// MarshalMap 变为可序列化的哈希表
func (c localCode) MarshalMap() map[string]interface{} {
	mashalMap := make(map[string]interface{})
	mashalMap["code"] = c.code
	mashalMap["message"] = c.message
	mashalMap["detail"] = c.detail

	return mashalMap
}

// String returns current error code as a string.
func (c localCode) String() string {
	if c.detail != nil {
		return fmt.Sprintf(`%d:%s %v`, c.code, c.message, c.detail)
	}
	if c.message != "" {
		return fmt.Sprintf(`%d:%s`, c.code, c.message)
	}
	return fmt.Sprintf(`%d`, c.code)
}
