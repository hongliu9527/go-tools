/*
 * @Author: hongliu
 * @Date: 2022-12-29 10:51:12
 * @LastEditors: hongliu
 * @LastEditTime: 2022-12-29 11:28:50
 * @FilePath: \go-tools\errors\gerror\gerror_error_code.go
 * @Description:带编码的错误处理
 *
 * Copyright (c) 2022 by 洪流, All Rights Reserved.
 */

package gerror

import (
	"gitee.com/hongliu9527/go-tools/errors/gcode"
)

// Code returns the error code.
// It returns CodeNil if it has no error code.
func (err *Error) Code() gcode.Code {
	if err == nil {
		return gcode.CodeNil
	}
	if err.code == gcode.CodeNil {
		return Code(err.Unwrap())
	}
	return err.code
}

// SetCode updates the internal code with given code.
func (err *Error) SetCode(code gcode.Code) {
	if err == nil {
		return
	}
	err.code = code
}
