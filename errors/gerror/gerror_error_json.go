/*
 * @Author: hongliu
 * @Date: 2022-12-29 10:51:12
 * @LastEditors: hongliu
 * @LastEditTime: 2022-12-29 11:29:30
 * @FilePath: \go-tools\errors\gerror\gerror_error_json.go
 * @Description:错误序列化
 *
 * Copyright (c) 2022 by 洪流, All Rights Reserved.
 */

package gerror

// MarshalJSON implements the interface MarshalJSON for json.Marshal.
// Note that do not use pointer as its receiver here.
func (err Error) MarshalJSON() ([]byte, error) {
	return []byte(`"` + err.Error() + `"`), nil
}
