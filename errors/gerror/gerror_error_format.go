/*
 * @Author: hongliu
 * @Date: 2022-12-29 10:51:12
 * @LastEditors: hongliu
 * @LastEditTime: 2022-12-29 11:29:11
 * @FilePath: \go-tools\errors\gerror\gerror_error_format.go
 * @Description:错误格式处理
 *
 * Copyright (c) 2022 by 洪流, All Rights Reserved.
 */

package gerror

import (
	"fmt"
	"io"
)

// Format formats the frame according to the fmt.Formatter interface.
//
// %v, %s   : Print all the error string;
// %-v, %-s : Print current level error string;
// %+s      : Print full stack error list;
// %+v      : Print the error string and full stack error list
func (err *Error) Format(s fmt.State, verb rune) {
	switch verb {
	case 's', 'v':
		switch {
		case s.Flag('-'):
			if err.text != "" {
				_, _ = io.WriteString(s, err.text)
			} else {
				_, _ = io.WriteString(s, err.Error())
			}
		case s.Flag('+'):
			if verb == 's' {
				_, _ = io.WriteString(s, err.Stack())
			} else {
				_, _ = io.WriteString(s, err.Error()+"\n"+err.Stack())
			}
		default:
			_, _ = io.WriteString(s, err.Error())
		}
	}
}
