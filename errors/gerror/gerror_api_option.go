/*
 * @Author: hongliu
 * @Date: 2022-12-29 10:51:12
 * @LastEditors: hongliu
 * @LastEditTime: 2022-12-29 11:27:09
 * @FilePath: \go-tools\errors\gerror\gerror_api_option.go
 * @Description: api错误创建选项
 *
 * Copyright (c) 2022 by 洪流, All Rights Reserved.
 */

package gerror

import "gitee.com/hongliu9527/go-tools/errors/gcode"

// Option is option for creating error.
type Option struct {
	Error error      // Wrapped error if any.
	Stack bool       // Whether recording stack information into error.
	Text  string     // Error text, which is created by New* functions.
	Code  gcode.Code // Error code if necessary.
}

// NewOption creates and returns a custom error with Option.
// It is the senior usage for creating error, which is often used internally in framework.
func NewOption(option Option) error {
	err := &Error{
		error: option.Error,
		text:  option.Text,
		code:  option.Code,
	}
	if option.Stack {
		err.stack = callers()
	}
	return err
}
