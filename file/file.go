/*
 * @Author: hongliu
 * @Date: 2023-01-09 17:30:48
 * @LastEditors: hongliu
 * @LastEditTime: 2023-01-09 17:35:07
 * @FilePath: \go-tools\file\file.go
 * @Description: 文件相关工具函数
 *
 * Copyright (c) 2023 by 洪流, All Rights Reserved.
 */

package file

import "os"

// Exists 判断文件/文件夹是否存在
func Exists(path string) bool {
	_, err := os.Stat(path) // os.Stat获取文件信息
	if err != nil {
		return os.IsExist(err)
	}

	return true
}
